using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public Button StartButton;
    public Text NameInput;
    public static string CharacterName;

    // Start is called before the first frame update
    void Start()
    {
        Button Btn = StartButton.GetComponent<Button>();
        Btn.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    // Update is called once per frame
    void Update()
    {
        CharacterName = NameInput.text;
    }
}
