using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public enum PlayerRoles
{
    Priest,
    Warrior,
    Rogue,
    Mage,
    Druid
}

public class PlayerData : MonoBehaviour
{

    public string PlayerName;
    public PlayerRoles Role;
    public float Height;
    public float Weight;
    public float Speed;
    public float TravelDistance;
    public Sprite[] AnimationSprites;
    private SpriteRenderer _sr;
    private int _counter;
    static public int FrameRate;
    private int _index;
    static public bool FacingRight = true;
    static public bool FacingLeft = false;
    private bool _stopAnimation = false;
    private float _timer = 0.0f;
    private float WaitingTime = 3.0f;

    // Start is called before the first frame update
    void Start()
    {
        _sr = gameObject.GetComponent<SpriteRenderer>();
        FrameRate = 120;
        _counter = 0;
        _index = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_stopAnimation) Animation();
        if (transform.position.x == 9.5f)
        {
            _stopAnimation = true;
            _timer +=  Time.deltaTime;
            if (_timer > WaitingTime)
            {
                _timer = 0f;
                Flip();
                FacingRight = false;
                FacingLeft = true;
                _stopAnimation = false;
            }
        }

        if (transform.position.x == -9.5f)
        {
            _stopAnimation = true;
            _timer += Time.deltaTime;
            if (_timer > WaitingTime)
            {
                _timer = 0f;
                Flip();
                FacingRight = true;
                FacingLeft = false;
                _stopAnimation = false;
            }
        }
    }

    void Flip()
    {
        Vector3 currentScale = gameObject.transform.localScale;
        currentScale.x *= -1;
        gameObject.transform.localScale = currentScale;

        FacingRight = !FacingRight;
    }

    void Animation()
    {
        if (_counter % FrameRate == 0)
        {
            _sr.sprite = AnimationSprites[_index];
            _counter = 0;
            _index = _index < AnimationSprites.Length - 1 ? (_index + 1) : 0;
        }
        _counter++;
    }
}
