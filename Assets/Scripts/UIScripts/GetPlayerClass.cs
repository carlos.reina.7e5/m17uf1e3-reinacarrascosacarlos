using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetPlayerClass : MonoBehaviour
{
    private GameObject _player;
    private PlayerData _playerData;
    private string _characterClass;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _playerData = _player.GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {
        _characterClass = _playerData.Role.ToString();
        this.gameObject.GetComponent<Text>().text = _characterClass;
    }
}
