using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiantAbility : MonoBehaviour
{

    public Button AbilityButton;
    private bool _btnPressed = false;
    private bool _cooldown = false;
    private bool _facingRight;
    private bool _facingLeft;

    // Start is called before the first frame update
    void Start()
    {
        Button Btn = AbilityButton.GetComponent<Button>();
        Btn.onClick.AddListener(ButtonPressed);
    }

    void ButtonPressed()
    {
        if (_cooldown == false)
        {
            _btnPressed = true;
            GameObject.Find("AbilityButton").GetComponentInChildren<Text>().text = "Cooldown...";
            Invoke("ResizeCharacter", 7.0f);
            Invoke("ResetCooldown", 17.0f);
            _cooldown = true;
        }
    }

    void ResetCooldown()
    {
        _cooldown = false;
        GameObject.Find("AbilityButton").GetComponentInChildren<Text>().text = "Click to grow";
    }

    // Update is called once per frame
    void Update()
    {
       if (_btnPressed)
        {
            GrowSizeAbility();  
            _btnPressed=false;
        }
       // _facingRight = gameObject.GetComponent<PlayerData>().FacingRight;
        _facingRight = PlayerData.FacingRight;
       // _facingLeft = gameObject.GetComponent<PlayerData>().FacingLeft;
        _facingLeft = PlayerData.FacingLeft;
    }

    void GrowSizeAbility()
    {
        if (_facingRight)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(13f, 13f, 13f);
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(16f, 16f, 16f);
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(19f, 19f, 19f);
        }

        else if (_facingLeft)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(-13f, 13f, 13f);
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(-16f, 16f, 16f);
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.45f, transform.position.z);
            transform.localScale = new Vector3(-19f, 19f, 19f);
        }
    }

    void ResizeCharacter()
    {
        if (_facingRight)
        {
            transform.position = new Vector3(0, -1.85f, 0);
            transform.localScale = new Vector3(10f, 10f, 10f);
        }
         if (_facingLeft)
        {
            transform.position = new Vector3(-0, -1.85f, 0);
            transform.localScale = new Vector3(-10f, 10f, 10f);
        }
    }
}
