using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudData : MonoBehaviour
{
    private GameObject _player;
    private PlayerData _playerData;
    private string _characterName;
    private string _characterClass;

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player");
        _playerData = _player.GetComponent<PlayerData>();
        _characterName = UIManager.CharacterName;
    }

    // Update is called once per frame
    void Update()
    {
        _playerData.PlayerName = _characterName;
        this.gameObject.GetComponent<Text>().text = _playerData.PlayerName;
        _characterClass = _playerData.Role.ToString();
    }
}
