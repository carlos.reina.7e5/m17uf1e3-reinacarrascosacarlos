using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float _speed;
    private float _weight;
    private float _finalSpeed;
    private float _distancia;
    private float _minX;
    private float _maxX;
    private bool _facingRight;
    private bool _facingLeft;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _speed = gameObject.GetComponent<PlayerData>().Speed;
        _weight = gameObject.GetComponent<PlayerData>().Weight;
        _finalSpeed = _speed - (_weight / 10);
        if (_finalSpeed < 0) _finalSpeed = 0.01f;
        _distancia = gameObject.GetComponent<PlayerData>().TravelDistance;
        _minX = _distancia * -1;
        _maxX = _distancia;
        //_facingRight = gameObject.GetComponent<PlayerData>().FacingRight;
        _facingRight = PlayerData.FacingRight;
        //_facingLeft = gameObject.GetComponent<PlayerData>().FacingLeft;
        _facingLeft = PlayerData.FacingLeft;
        //transform.position = new Vector3(_finalSpeed * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);
        //transform.Translate(Vector3.right * Time.deltaTime * _finalSpeed, 0);
        //_movimientoX = Input.GetAxis("Horizontal");
        if (_facingRight) transform.Translate(Vector3.right * Time.deltaTime * _finalSpeed, 0);
        if (_facingLeft) transform.Translate(Vector3.left * Time.deltaTime * _finalSpeed, 0);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, _minX, _maxX), transform.position.y, transform.position.z);
    }

    
}
